import { Component, OnInit } from '@angular/core';
import { HeroesService } from "../servicios/heroes.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: any;

  constructor(private router:Router, private heroesService:HeroesService) { }

  ngOnInit() {
    
  //  this.heroes = this.heroesService.getHeroes();
  this.heroesService.getHeroesAPI()
  .subscribe(data => {
    console.log(data)
    this.heroes = data.results;
  },
     error => {
       console.log("fallo el call de la API");
     
       console.log(error)
     });

    console.log(this.heroes);
  
  }

  verHeroe(idx:number) {
    console.log(idx);
    this.router.navigate(['/heroe',idx]);
  }
 
}
